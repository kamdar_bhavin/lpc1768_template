#ifndef _DELAY_H
#define _DELAY_H

void delay_ms(unsigned int count);
void delay_us(unsigned int count);

#endif
