# put your *.o targets here, make should handle the rest!
SRCS = $(wildcard src/*.c)
SRCS += $(wildcard libraries/*.c)
SRCS += $(wildcard libraries/CMSIS/*.c)
SRCS += $(wildcard device/*.c)

# all the files will be generated with this name (main.elf, main.bin, main.hex, etc)
PROJ_NAME=blinky

# Location of the Libraries folder from the STM32F0xx Standard Peripheral Library
STD_PERIPH_LIB=libraries

# Location of the linker scripts
LDSCRIPT_INC=device/ldscripts

# location of OpenOCD Board .cfg files (only used with 'make program')
OPENOCD_TARGET_DIR=$(OPENOCD_PATH)/../scripts/target
OPENOCD_INTERFACE_DIR=$(OPENOCD_PATH)/../scripts/interface

# Configuration (cfg) file containing programming directives for OpenOCD
OPENOCD_PROC_FILE=extra/lpc1768-openocd.cfg

OPENOCD=$(OPENOCD_PATH)/openocd

# that's it, no need to change anything below this line!

###################################################

CC=arm-none-eabi-gcc
OBJCOPY=arm-none-eabi-objcopy
OBJDUMP=arm-none-eabi-objdump
SIZE=arm-none-eabi-size

CFLAGS  = -Wall -g -std=c99 -O0
CFLAGS += -mlittle-endian -mcpu=cortex-m0  -march=armv6-m -mthumb
CFLAGS += -ffunction-sections -fdata-sections
CFLAGS += -Wl,--gc-sections -Wl,-Map=$(PROJ_NAME).map

###################################################

vpath %.c src
#vpath %.a $(STD_PERIPH_LIB)

ROOT=$(shell pwd)

CFLAGS += -I inc -I $(STD_PERIPH_LIB)

OBJS = $(SRCS:.c=.o)

###################################################

.PHONY: lib proj

all: proj

proj: 	$(PROJ_NAME).elf

$(PROJ_NAME).elf: $(SRCS)
	$(CC) $(CFLAGS) $^ -o $@ -L$(LDSCRIPT_INC) -TLPC1768-flash.ld
	$(OBJCOPY) -O ihex $(PROJ_NAME).elf $(PROJ_NAME).hex
	$(OBJCOPY) -O binary $(PROJ_NAME).elf $(PROJ_NAME).bin
	$(OBJDUMP) -St $(PROJ_NAME).elf >$(PROJ_NAME).lst
	$(SIZE) $(PROJ_NAME).elf
	
program: $(PROJ_NAME).elf
	$(OPENOCD) -f $(OPENOCD_INTERFACE_DIR)/ngxtech.cfg -f $(OPENOCD_TARGET_DIR)/lpc1768.cfg -f $(OPENOCD_PROC_FILE) -c "lpc_flash `pwd`/$(PROJ_NAME).elf" -c shutdown

clean:
	find ./ -name '*~' | xargs rm -f	
	rm -rf *.o
	rm -rf $(PROJ_NAME).elf
	rm -rf $(PROJ_NAME).hex
	rm -rf $(PROJ_NAME).bin
	rm -rf $(PROJ_NAME).map
	rm -rf $(PROJ_NAME).lst

reallyclean: clean
	$(MAKE) -C $(STD_PERIPH_LIB) clean
