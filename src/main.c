#include "CMSIS/LPC17xx.h"
#include "delay.h"

void ports_init()
{
  //currently initializes everything as GPIOs, will change later
	LPC_PINCON->PINSEL0 = 0;
	LPC_PINCON->PINSEL1 = 0;
	LPC_PINCON->PINSEL2 = 0;
	LPC_PINCON->PINSEL3 = 0;
	LPC_PINCON->PINSEL4 = 0;
	LPC_PINCON->PINSEL5 = 0;
	LPC_PINCON->PINSEL6 = 0;
	LPC_PINCON->PINSEL7 = 0;
	LPC_PINCON->PINSEL8 = 0;
	LPC_PINCON->PINSEL9 = 0;
	LPC_PINCON->PINSEL10 = 0;   
}


int main(void )
{
  ports_init();
  LPC_GPIO0->FIODIR |= (7 << 24);           /* LEDs on PORT0 */
  while(1)
  {
    LPC_GPIO0->FIOSET = (7 << 24);
    delay_ms(2000);
    LPC_GPIO0->FIOCLR = (7 << 24);
    delay_ms(500);
  }	

}


